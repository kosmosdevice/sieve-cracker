import decimal
import math
import os
import time
import numpy as np


def trial_division(n: int):
    res = []
    while n % 2 == 0:
        res.append(2)
        n //= 2
        f = 3
    while f * f <= n:
    if n % f == 0:
        res.append(f)
        n //= f
    else:
        f += 2
    if n != 1:
        res.append(n)
return res


def gcd(a, b):
    if a > b:
        r_prev = a
        r_current = b
    else:
        r_prev = b
        r_current = a
        i = 1
    while r_current != 0:
        r_next = r_prev % r_current
        r_prev = r_current
        r_current = r_next
        i += 1
return r_prev


def generate_primes(n: int):
    primes = []
    file = open("prim_2_24.txt", "r")
    for line in file:
        for num in line.split():
            if len(primes) >= n:
                return primes
            primes.append(int(num))
return primes


def is_smooth(r2, f):
    temp = r2
    factors = {}
    if r2 == 0:
        return -1
    for i in f:
        while temp % i == 0:
            temp /= i
            try:
                factors[i] += 1
            except KeyError:
                factors[i] = 1
    if temp == 1:
        return factors
return -1


def generate_matrix_r(n: int, b: int, l: int, f):
    r_list = []
    counter = 0
    for k in range(1, l):
        for j in range(k):
            if counter == l:
                break
            r = int(math.floor(math.sqrt(k * n)) + j)
            r2 = pow(r, 2, n)
            factors = is_smooth(r2, f)
            if factors != -1:
                r2_binary = np.zeros([1, len(f)], dtype=int)
                for key, val in factors.items():
                    idx = f.index(key)
                    r2_binary[0][idx] = val % 2
                try:
                    if not ((m == r2_binary).all(1).any()):
                        m = np.vstack([m, r2_binary])
                        r_list.append(r)
                        counter += 1
                except UnboundLocalError:
                    m = r2_binary
                    r_list.append(r)
                    counter += 1
return m, r_list


def find_solution(rs_list, sols, n):
    decimal.getcontext().prec = int(pow(2, 12))
    for s in sols:
        lhs = decimal.Decimal(1) # tracks x
        rhs = decimal.Decimal(1) # track y^2
        for idx, val in enumerate(s):
            if val == 1:
            r = rs_list[idx]
            lhs = lhs * r % n
            r2 = pow(r, 2, n)
            rhs = rhs * decimal.Decimal(r2)
    rhs = int(decimal.Decimal(rhs.sqrt())) % n
    p = gcd(abs(rhs - lhs), n)
    q = n / p
return min(p, q), max(p, q)


def gauss_bin(m, rs_list, f):
    inp = open('inp.in', 'w')
    inp.write(str(len(rs_list)))
    inp.write(" ")
    inp.write(str(len(f)))
    inp.write("\n")
    for row in m:
        for col in row:
            inp.write(str(col))
            inp.write(" ")
        inp.write("\n")
    inp.close()
    os.system("GaussBin.exe {} {}".format("inp.in", "out.in"))
    out = open("out.in", "r")
    solutions = int(out.readline())
    sols = []
    for i in range(solutions):
        line = out.readline()
        res = []
        for j in line:
            if j != ' ' and j != '\n':
                res.append(int(j))
        sols.append(res)
return sols


def print_execution_time(start, interval1, interval2, interval3, end, totaltime):
print("Execution times:")
print("Generating primes:", "{:.1f}".format((interval1 - start) * 10 ** 3),
"ms", "{:.1f}".format((interval1 - start) / totaltime * 100), "%")
print("Generating rs and matrix:", "{:.1f}".format((interval2 - interval1) *
10 ** 3),↪→
"ms", "{:.1f}".format((interval2 - interval1) / totaltime * 100), "%")
print("Running GaussBin:", "{:.1f}".format((interval3 - interval2) * 10 **
3),↪→
"ms", "{:.1f}".format((interval3 - interval2) / totaltime * 100), "%")
print("Finding solution:", "{:.1f}".format((end - interval3) * 10 ** 3),
"ms", "{:.1f}".format((end - interval3) / totaltime * 100), "%")
print("Total:", "{:.1f}".format((end - start) * 10 ** 3), "ms")


def quadratic_sieve(n: int, l, f_size):
    start = time.time()
    f = generate_primes(f_size)
    b = f[-1] + 1
    interval1 = time.time()
    m, rs_list = generate_matrix_r(n, b, l, f)
    interval2 = time.time()
    sols = gauss_bin(m, rs_list, f)
    interval3 = time.time()
    p, q = find_solution(rs_list, sols, n)
    end = time.time()
    total = end - start
    if p != 1 and q != 1:
        return str(n) + " = " + str(p) + " * " + str(q)
    else:
        return "Solution not found!"


l = 1020
f = 1000
print(quadratic_sieve(323, 5, 4))
print(quadratic_sieve(16637, 12, 10))
print(quadratic_sieve(307561, 30, 20))
print(quadratic_sieve(31741649, 30, 20))
print(quadratic_sieve(16843009, 100, 80))
print(quadratic_sieve(3205837387, 100, 80))
print(quadratic_sieve(392742364277, l, f))
print(quadratic_sieve(220744554721994695419563, l, f))
